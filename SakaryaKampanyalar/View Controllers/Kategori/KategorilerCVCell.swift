//
//  KategorilerCollectionViewCell.swift
//  testPostJson
//
//  Created by Yunus Tek on 15/10/2017.
//  Copyright © 2017 Yunus Tek. All rights reserved.
//

import UIKit

class KategorilerCVCell: UICollectionViewCell {
    
    @IBOutlet weak var arkaPlanResmi: UIImageView!
    @IBOutlet weak var resmi: UIImageView!
    @IBOutlet weak var adi: UILabel!
    
}
