//
//  GizlilikPolitikasiVC.swift
//  SakaryaKampanyalar
//
//  Created by Yunus Tek on 22.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class GizlilikPolitikasiVC: UIViewController {
    
    @IBOutlet weak var textField: UITextView!
    var docController:UIDocumentInteractionController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.textField.scrollRangeToVisible(NSMakeRange(0,0))
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}

