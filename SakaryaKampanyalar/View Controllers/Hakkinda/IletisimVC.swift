//
//  IletisimVC.swift
//  SakaryaKampanyalar
//
//  Created by Yunus Tek on 22.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class IletisimVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPhone(_ sender: Any) {
        GlobalVariables.open("tel://02642816555")
    }
    
    @IBAction func btnAdress(_ sender: Any) {
        let addressString: String = "http://maps.apple.com/?q="
            + "Yenido%C4%9Fan+Mah.+Fabrika+Cad.+No:+35+Adapazari+Sakarya";
        
//        self.firmaAdres.text!.replaceTr(text: self.firmaAdres.text!)
        
        GlobalVariables.open(addressString)
    }
    
}

