//
//  HakkindaVC.swift
//  SakaryaKampanyalar
//
//  Created by Yunus Tek on 20.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class HakkindaVC: UIViewController {

    @IBOutlet weak var mynavigationItem: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mynavigationItem.leftBarButtonItem = GlobalVariables.shared.newBarButton("menu", action: #selector(SWRevealViewController.revealToggle(_:)), view: self.revealViewController())

        GlobalVariables.menuView(current: self)
    }
    
}
