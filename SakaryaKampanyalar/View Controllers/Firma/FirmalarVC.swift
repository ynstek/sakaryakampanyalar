//
//  FirmalarVC.swift
//  SakaryaKampanyalar
//
//  Created by Yunus Tek on 12.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class FirmalarVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblBaslik: UILabel!
    
    var kategoriId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if GlobalVariables.shared.isFavorilerim {
            self.lblBaslik.text = "Favorilerim" // Favorilerim
            presentTransparentNavigationBar()
            
            self.navigationItem.leftBarButtonItem =
                GlobalVariables.shared.newBarButton("menu", action: #selector(SWRevealViewController.revealToggle(_:)), view: self.revealViewController())
        } else {
            self.lblBaslik.text = "Firmalar" // Firmalar
            NotificationCenter.default.addObserver(self, selector: #selector(FirmalarVC.reloadFirma(_:)), name: NSNotification.Name(rawValue: "reloadFirma"), object: nil)

            // Filter
            self.navigationItem.rightBarButtonItem =
                GlobalVariables.shared.newBarButton("filter", action: #selector(SWRevealViewController.rightRevealToggle(_:)), view: self.revealViewController())
        }
        
        self.getDATA()
    }
    
    @objc func reloadFirma(_ notification: Notification){
        self.getDATA()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GlobalVariables.menuView(current: self, right: true)
    }

    func getDATA() {
        if Reachability.isConnectedToNetwork() {
            GlobalVariables.openActivity()
            if GlobalVariables.shared.isFavorilerim {
                GlobalVariables.shared.jsonFavorilerim.removeAll()
                if let data = UserDefaults.standard.value(forKey: "Favorilerim") as? Data {
                    GlobalVariables.shared.jsonFavorilerim = try! PropertyListDecoder().decode(Array<JsonFirma.Value>.self, from: data)
                }
                self.collectionView.reloadData()
                GlobalVariables.closeActivity()
            } else {
                GlobalVariables.shared.jsonFirmalar.removeAll()
                JsonFirma.Todo.allTodos(kategoriId) { (result, error) in
                    if error == nil {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            GlobalVariables.shared.jsonFirmalar = result!
                            
                            if let il = UserDefaults.standard.object(forKey: "IL") as? String,
                                let ilce = UserDefaults.standard.object(forKey: "ILCE") as? String
                            {
                                GlobalVariables.shared.jsonFirmalar = result!.filter() {
                                    f in
                                    return f.FirmaIli == il && f.FirmaIlcesi == ilce
                                }
                            }
                            
                            self.collectionView.reloadData()
                            GlobalVariables.closeActivity()
                        }
                        
                    } else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            GlobalVariables.closeActivity()
                        }
                    }
                }
            }
        }
    }
    
    // MARK: CollectionView
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.width - 24) / 2, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if GlobalVariables.shared.isFavorilerim {
            return GlobalVariables.shared.jsonFavorilerim.count
        } else {
            return GlobalVariables.shared.jsonFirmalar.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FirmalarCVCell
        var data: JsonFirma.Value?
        
        if GlobalVariables.shared.isFavorilerim {
            data = GlobalVariables.shared.jsonFavorilerim[indexPath.row]
        } else {
            data = GlobalVariables.shared.jsonFirmalar[indexPath.row]
        }
        
        cell.adi.text = data!.Adi
        cell.logo.image = UIImage(named: "noimage")

        if let logo = data!.Logo {
            cell.logo.downloadedFrom(link: logo)
        }
        
        // For Favorilerim
        if let dataFav = UserDefaults.standard.value(forKey: "Favorilerim") as? Data {
            let favorilerim = try! PropertyListDecoder().decode(Array<JsonFirma.Value>.self, from: dataFav)
            
            var id: [JsonFirma.Value] = []
            id = favorilerim.filter() {
                favorilerim in
                return favorilerim.Id == data!.Id
            }
            
            if id.count != 0 {
                cell.isFav = true
                cell.imgFav.image = UIImage(named: "fav1")
            } else {
                cell.isFav = false
                cell.imgFav.image = UIImage(named: "fav0")
            }
        } else {
            cell.isFav = false
            cell.imgFav.image = UIImage(named: "fav0")
        }
        
        cell.firma = data
        
        return cell
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let cell = sender as! UICollectionViewCell
        let indexPath = self.collectionView!.indexPath(for: cell)!
        
        let vc = segue.destination as! UrunlerVC
        if GlobalVariables.shared.isFavorilerim {
            vc.firmaID = GlobalVariables.shared.jsonFavorilerim[indexPath.row].Id!
        } else {
            vc.firmaID = GlobalVariables.shared.jsonFirmalar[indexPath.row].Id!
        }
        
    }
    
    func presentTransparentNavigationBar() {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
        self.navigationController!.navigationBar.isTranslucent = true
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.setNavigationBarHidden(false, animated:true)
        self.navigationItem.hidesBackButton = true
        //        self.edgesForExtendedLayout = UIRectEdge.None // Kenarlar
        //        self.navigationController!.navigationBar.barTintColor = GlobalFunctions.shared.getColor("black")
    }

}
