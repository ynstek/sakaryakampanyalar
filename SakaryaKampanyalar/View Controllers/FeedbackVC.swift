//
//  FeedbackVC.swift
//  SakaryaKampanyalar
//
//  Created by Yunus Tek on 16.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class FeedbackVC: UIViewController {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var txtAd: UITextField!
    @IBOutlet weak var txtSoyad: UITextField!
    @IBOutlet weak var txtEposta: UITextField!
    @IBOutlet weak var txtTelefon: UITextField!
    @IBOutlet weak var txtMesaj: UITextView!
    
    @IBOutlet weak var txtKonu: UITextField!
    
    var firmaId = 0
    
    @IBOutlet weak var mynavigationItem: UINavigationItem!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
    }

    override func viewWillAppear(_ animated: Bool) {
        if firmaId == 0 {
            self.mynavigationItem.leftBarButtonItem =
                GlobalVariables.shared.newBarButton("menu", action: #selector(SWRevealViewController.revealToggle(_:)), view: self.revealViewController())
        }
        
        GlobalVariables.menuView(current: self)
    }
    
    @IBAction func btnSend(_ sender: Any) {
        if isValid() {
            self.sendMessage()
        }
    }
    
    func sendMessage() {
        // POST JSON
        if Reachability.isConnectedToNetwork() {
            GlobalVariables.openActivity()
            
            JsonFeedback.Todo.save(Adi: txtAd.text!, Soyadi: txtSoyad.text!, Konu: txtKonu.text!, Mesaj: txtMesaj.text!, FirmaId: firmaId, Tel: txtTelefon.text!, Email: txtEposta.text!, completionHandler: { (result, error) in
                
                GlobalVariables.closeActivity()
                if error != nil {
                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: error!.localizedDescription)
                } else {
                    if result?.keys.first == "message" {
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: result!.values.first!)
                    } else { // "sonuc"
                        AlertFunctions.messageType.showOKAlert("Mesajınız İletildi", bodyMessage: "En kısa zamanda E-Mail adresinize geri dönüş sağlanacaktır")
                    }
                }
            })
            
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    func isValid () -> Bool {
        // Bos
        if !txtAd.text!.isEmpty && !txtSoyad.text!.isEmpty
            && !txtEposta.text!.isEmpty && !txtTelefon.text!.isEmpty
            && !txtMesaj.text!.isEmpty
        {
            
            // Email, Phone, Password check
            if  GlobalVariables.isValidEmail(txtEposta.text!)
                && GlobalVariables.isValidPhone(txtTelefon.text!)
            {
                return true
            } else {
                return false
            }
            
        } else {
            AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Lütfen tüm bilgileri doldurduğunuzdan emin olunuz.")
            return false
        }
    }

    @IBAction func ChangePhoneText(_ sender: Any) {
        if (self.txtTelefon.text!.isEmpty) {
            self.txtTelefon.text! = "+"
        }
    }
    
}
