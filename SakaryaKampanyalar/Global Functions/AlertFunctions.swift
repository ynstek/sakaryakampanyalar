//
//  AlertFunctions.swift
//  Orca POS Mobile
//
//  Created by Yunus TEK on 21.03.2017.
//  Copyright © 2017 Orca Businesss Solutions. All rights reserved.
//

import UIKit
import Foundation
import CoreData

private let _sharedAlertFunctions = AlertFunctions()
var progressView: UIProgressView?
var progressItemStatus: UILabel?
var progressItemUnitsStatus: UILabel?
var progressCustomerStatus: UILabel?
var progressBalanceHistoryStatus: UILabel?
var pending = UIAlertController()
var totalCount = 0
var indicator: UIActivityIndicatorView?
class AlertFunctions : NSObject  {
    
    // MARK: - SHARED INSTANCE
    class var messageType : AlertFunctions {
        return _sharedAlertFunctions
    }
    
    func showYesNoAlert(_ titleMessage: String, bodyMessage: String, _ yes: @escaping () -> Void, no: @escaping () -> Void){
        
        let alertController = UIAlertController(title: titleMessage, message: bodyMessage, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Evet", style: .default , handler: { (action: UIAlertAction!) in
            yes()
            //            return true
            print("Yes")
        }))
        
        alertController.addAction(UIAlertAction(title: "Hayır", style: .cancel , handler: {(action: UIAlertAction!) in
            no()
            //            return true
            print("No")
        }))
        
        GlobalVariables.getTopController().present(alertController, animated:true, completion:nil)
    }
    
    func showOKAlert(_ titleMessage: String, bodyMessage: String) {
        
        let alertController = UIAlertController(title: titleMessage, message: bodyMessage, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "Tamam", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        GlobalVariables.getTopController().present(alertController, animated:true, completion:nil)
    }
    
    func showOKAlertVoid(_ titleMessage: String, bodyMessage: String, _ tamam: @escaping () -> Void){
        
        let alertController = UIAlertController(title: titleMessage, message: bodyMessage, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Tamam", style: .default , handler: { (action: UIAlertAction!) in
            tamam()
        }))
        
        GlobalVariables.getTopController().present(alertController, animated:true, completion:nil)
    }
    
    func showInfoAlert(_ titleMessage: String, bodyMessage: String){
        
        let alertController = UIAlertController(title: titleMessage, message: bodyMessage, preferredStyle: .alert)
        GlobalVariables.getTopController().present(alertController, animated:true, completion:nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            alertController.dismiss(animated: false, completion: nil)
        }
    }
    
    func showYesNoAlertWithSwitch(_ titleMessage: String, bodyMessage: String, _ switchTitle: String, _ switchStatus: Bool, success: @escaping (_ status: Bool) -> Void) {
        // private Alert
        let alert = UIAlertController(title: titleMessage, message: bodyMessage + "\n\n", preferredStyle: .alert)
        
        // Create Label
        let labelFrame: CGRect = CGRect(x: 65, y: 75, width: 150, height: 20)
        let label: UILabel! = UILabel(frame: labelFrame)
        label.text = switchTitle
        alert.view.addSubview(label)
        
        // Create Switch Object
        let switchFrame: CGRect = CGRect(x: 160, y: 70, width: 20, height: 20)
        let switchObject: UISwitch! = UISwitch(frame: switchFrame)
        switchObject.isOn = switchStatus
        
        alert.view.addSubview(switchObject)
        
        alert.addAction(UIAlertAction(title: "Evet", style: .default , handler: { (alert) in
            print("Yes", "Show Price:", switchObject.isOn)
            success(switchObject.isOn)
        }))
        
        alert.addAction(UIAlertAction(title: "Hayır", style: .cancel , handler: {(alert) in
            print("No")
        }))
        
        GlobalVariables.getTopController().present(alert, animated: true, completion: nil)
    }
    
    func datePickerAlert (title: String, pickerdate: Date, minimumdate: Date?, apply: @escaping (_ date: Date) -> Void, cancel: @escaping () -> Void) {
        
        let alert = UIAlertController(title: title, message: "\n\n\n\n\n\n\n\n", preferredStyle: .alert)
        alert.view.subviews.first!.backgroundColor = UIColor.white
        
        // Clock cover: View
        let viewFrame: CGRect = CGRect(x: 198, y: 65, width: 52, height: 150)
        let view: UIView = UIView(frame: viewFrame)
        view.backgroundColor = UIColor.colorWithRedValue(248, greenValue: 248, blueValue: 248, alpha: 1)
        alert.view.subviews.first!.layer.cornerRadius = 8
        alert.view.subviews.first!.layer.masksToBounds = true
        
        // Date Picker
        let pickerFrame: CGRect = CGRect(x: 70, y: 55, width: 150, height: 160)
        let picker: UIDatePicker = UIDatePicker(frame: pickerFrame)
        if minimumdate != nil {
            picker.minimumDate = minimumdate
        }
        picker.locale = Locale(identifier: "de") // germany
        picker.date = pickerdate
        
        alert.view.addSubview(picker)
        alert.view.addSubview(view)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler: {(alert) in
            cancel()
        }))
        
        alert.addAction(UIAlertAction(title: "Apply", style: .default , handler: { (alert) in
            apply(picker.date)
        }))
        
        GlobalVariables.getTopController().present(alert, animated: true, completion: nil)
    }

}


